import socket
import _pickle
import threading


class Game:
    def __init__(self, color=None):
        self.board = Board()

        self.round = 5
        self.winner = None

        self.turn = 'black'
        if color is None:
            self.rival_disc = 'white'
        else:
            self.my_disc = None
            self.rival_disc = None
            self.init_discs(color)

    def init_discs(self, color):
        if color == 'black':
            self.my_disc, self.rival_disc = 'black', 'white'
        else:
            self.my_disc, self.rival_disc = 'white', 'black'

    def change_turn(self):
        if self.winner is None:
            self.turn, self.rival_disc = self.rival_disc, self.turn
            if not self.has_chance():
                self.turn, self.rival_disc = self.rival_disc, self.turn
            self.winner = self.is_end()
            self.round += 1

    def put(self, square_loc):
        grabbed_line = []
        if self.board.is_none(square_loc):
            grabbed_line.extend(self.board.check_left(square_loc, self.turn))
            grabbed_line.extend(self.board.check_right(square_loc, self.turn))
            grabbed_line.extend(self.board.check_up(square_loc, self.turn))
            grabbed_line.extend(self.board.check_down(square_loc, self.turn))
            grabbed_line.extend(self.board.check_left_up(square_loc, self.turn))
            grabbed_line.extend(self.board.check_left_down(square_loc, self.turn))
            grabbed_line.extend(self.board.check_right_up(square_loc, self.turn))
            grabbed_line.extend(self.board.check_right_down(square_loc, self.turn))
            if len(grabbed_line) != 0:
                grabbed_discs = self.grab(grabbed_line, square_loc)
                return grabbed_discs

    def grab(self, grabbed_line, put_disc_loc):
        disc_loc = []
        for line_loc in grabbed_line:
            # horizontal
            if put_disc_loc[0] == line_loc[0]:
                if put_disc_loc[1] > line_loc[1]:
                    for disc_col in range(line_loc[1] + 1, put_disc_loc[1]):
                        disc_loc.append((put_disc_loc[0], disc_col))
                else:
                    for disc_col in range(put_disc_loc[1] + 1, line_loc[1]):
                        disc_loc.append((put_disc_loc[0], disc_col))
                continue
            elif put_disc_loc[1] == line_loc[1]:
                if put_disc_loc[0] > line_loc[0]:
                    for disc_row in range(line_loc[0] + 1, put_disc_loc[0]):
                        disc_loc.append((disc_row, put_disc_loc[1]))
                else:
                    for disc_row in range(put_disc_loc[0] + 1, line_loc[0]):
                        disc_loc.append((disc_row, put_disc_loc[1]))
                continue
            else:
                i = 1
                if put_disc_loc[0] > line_loc[0]:
                    if put_disc_loc[1] > line_loc[1]:
                        for disc_row in range(line_loc[0] + 1, put_disc_loc[0]):
                            disc_loc.append((disc_row, line_loc[1] + i))
                            i += 1
                    else:
                        for disc_row in range(line_loc[0] + 1, put_disc_loc[0]):
                            disc_loc.append((disc_row, line_loc[1] - i))
                            i += 1
                else:
                    if put_disc_loc[1] > line_loc[1]:
                        for disc_row in range(put_disc_loc[0] + 1, line_loc[0]):
                            disc_loc.append((disc_row, put_disc_loc[1] - i))
                            i += 1
                    else:
                        for disc_row in range(put_disc_loc[0] + 1, line_loc[0]):
                            disc_loc.append((disc_row, put_disc_loc[1] + i))
                            i += 1
        self.board.change_disc(put_disc_loc, self.turn)
        for disc in disc_loc:
            self.board.change_disc(disc, self.turn)

        return put_disc_loc, disc_loc

    def has_chance(self):
        for row in range(8):
            for col in range(8):
                if self.board.playground[row][col].color is None:
                    if len(self.board.check_up((row, col), self.turn)) != 0:
                        return True
                    elif len(self.board.check_left((row, col), self.turn)) != 0:
                        return True
                    elif len(self.board.check_down((row, col), self.turn)) != 0:
                        return True
                    elif len(self.board.check_right((row, col), self.turn)) != 0:
                        return True
                    elif len(self.board.check_left_up((row, col), self.turn)) != 0:
                        return True
                    elif len(self.board.check_right_up((row, col), self.turn)) != 0:
                        return True
                    elif len(self.board.check_left_down((row, col), self.turn)) != 0:
                        return True
                    elif len(self.board.check_right_down((row, col), self.turn)) != 0:
                        return True
        return False

    def is_full(self):
        if self.round == 64:
            return True
        return False

    def is_end(self):
        if not self.board.has_disc(self.rival_disc):
            return self.turn
        elif self.is_full():
            black_num, white_num = self.board.count_disc()
            winner = self.determine_winner(black_num, white_num)
            return winner

    @staticmethod
    def determine_winner(black_num, white_num):
        if black_num > white_num:
            return 'black'
        elif black_num < white_num:
            return 'white'
        else:
            return 'draw'


class Board:
    def __init__(self):
        self.playground = [[Disc(None) for col in range(8)] for row in range(8)]
        self.playground[3][3].color = 'black'
        self.playground[4][4].color = 'black'
        self.playground[3][4].color = 'white'
        self.playground[4][3].color = 'white'

    def check_left(self, loc, turn):
        grabbed_line = []
        if loc[1] > 1:
            for col in range(loc[1] - 1, -1, -1):
                if self.playground[loc[0]][col].color == turn:
                    if col != loc[1] - 1:
                        grabbed_line.append((loc[0], col))
                    break
                elif self.playground[loc[0]][col].color is None:
                    break
        return grabbed_line

    def check_right(self, loc, turn):
        grabbed_line = []
        if loc[1] < 6:
            for col in range(loc[1] + 1, 8):
                if self.playground[loc[0]][col].color == turn:
                    if col != loc[1] + 1:
                        grabbed_line.append((loc[0], col))
                    break
                elif self.playground[loc[0]][col].color is None:
                    break
        return grabbed_line

    # vertical
    def check_up(self, loc, turn):
        grabbed_line = []
        if loc[0] > 1:
            for row in range(loc[0] - 1, -1, -1):
                if self.playground[row][loc[1]].color == turn:
                    if row != loc[0] - 1:
                        grabbed_line.append((row, loc[1]))
                    break
                elif self.playground[row][loc[1]].color is None:
                    break
        return grabbed_line

    def check_down(self, loc, turn):
        grabbed_line = []
        if loc[0] < 6:
            for row in range(loc[0] + 1, 8):
                if self.playground[row][loc[1]].color == turn:
                    if row != loc[0] + 1:
                        grabbed_line.append((row, loc[1]))
                    break
                elif self.playground[row][loc[1]].color is None:
                    break
        return grabbed_line

    def check_left_up(self, loc, turn):
        grabbed_line = []
        if loc[0] != 0 and loc[1] != 0:
            row = loc[0] - 1
            col = loc[1] - 1
            while row >= 0 and col >= 0:
                if self.playground[row][col].color == turn:
                    if row != loc[0] - 1 and col != loc[1] - 1:
                        grabbed_line.append((row, col))
                    break
                elif self.playground[row][col].color is None:
                    break
                row -= 1
                col -= 1
        return grabbed_line

    def check_left_down(self, loc, turn):
        grabbed_line = []
        if loc[0] != 7 and loc[1] != 0:
            row = loc[0] + 1
            col = loc[1] - 1
            while row <= 7 and col >= 0:
                if self.playground[row][col].color == turn:
                    if row != loc[0] + 1 and col != loc[1] - 1:
                        grabbed_line.append((row, col))
                    break
                elif self.playground[row][col].color is None:
                    break
                row += 1
                col -= 1
        return grabbed_line

    def check_right_up(self, loc, turn):
        grabbed_line = []
        if loc[0] != 0 and loc[1] != 7:
            row = loc[0] - 1
            col = loc[1] + 1
            while row >= 0 and col <= 7:
                if self.playground[row][col].color == turn:
                    if row != loc[0] - 1 and col != loc[1] + 1:
                        grabbed_line.append((row, col))
                    break
                elif self.playground[row][col].color is None:
                    break
                row -= 1
                col += 1
        return grabbed_line

    def check_right_down(self, loc, turn):
        grabbed_line = []
        if loc[0] != 7 and loc[1] != 7:
            row = loc[0] + 1
            col = loc[1] + 1
            while row <= 7 and col <= 7:
                if self.playground[row][col].color == turn:
                    if row != loc[0] + 1 and col != loc[1] + 1:
                        grabbed_line.append((row, col))
                    break
                elif self.playground[row][col].color is None:
                    break
                row += 1
                col += 1
        return grabbed_line

    def change_disc(self, disc_loc, turn):
        self.playground[disc_loc[0]][disc_loc[1]].change_color(turn)

    def has_disc(self, rival_disc):
        for row in range(7):
            for col in range(7):
                if self.playground[row][col].color == rival_disc:
                    return True
        return False

    def count_disc(self):
        black_num, white_num = 0, 0

        for row in range(8):
            for col in range(8):
                if self.playground[row][col].color == 'black':
                    black_num += 1
                elif self.playground[row][col].color == 'white':
                    white_num += 1
        return black_num, white_num

    def is_none(self, square_loc):
        if self.playground[square_loc[0]][square_loc[1]].color is None:
            return True
        return False


class Disc:
    def __init__(self, color):
        self.color = color

    def change_color(self, color):
        self.color = color


class Connector:
    def __init__(self, play_online_controller):
        self.IP_ADDRESS = '127.0.0.1'
        self.PORT_NUMBER = 9876
        self.BUFFER_SIZE = 256

        self.play_online_controller = play_online_controller
        self.socket = socket.socket()
        self.data = None
        self.close = False

    def connect(self):
        self.socket.connect((self.IP_ADDRESS, self.PORT_NUMBER))
        threading.Thread(target=self.receive).start()

    def send(self, data):
        msg = _pickle.dumps(data, -1)
        self.socket.send(msg)

    def receive(self):
        while not self.close:
            try:
                self.data = _pickle.loads(self.socket.recv(256))
                self.play_online_controller.check_data(self.data)
            except EOFError:
                self.close_connector()

    def close_connector(self):
        self.close = True
        self.data = None
        self.play_online_controller = None
        self.socket.close()
