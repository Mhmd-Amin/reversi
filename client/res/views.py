from tkinter import *

from PIL import ImageTk, Image


class Menu:
    def __init__(self, menu_controller):
        self.menu_controller = menu_controller
        self.root = Tk()

        self.view_height = self.root.winfo_screenheight() - 67
        screen_width = self.root.winfo_screenwidth()
        self.view_width = self.view_height * 8 // 9

        self.root.title('Reversi')
        self.root.geometry(f'{self.view_width}x{self.view_height}+{screen_width // 2 - self.view_width // 2}+{0}')
        self.root.resizable(width=False, height=False)
        self.root.configure(bg='#004c4c')

        self.widget_width = self.view_height * 2 // 3

        # canvas territory
        self.logo_canvas = Canvas(self.root,
                                  width=self.widget_width,
                                  height=self.widget_width,
                                  bg='#004c4c',
                                  bd=0,
                                  highlightthickness=0)

        # image territory
        primary_logo = Image.open('res/logo.png')
        resized_logo = primary_logo.resize((self.widget_width, self.widget_width), Image.ANTIALIAS)
        self.final_logo = ImageTk.PhotoImage(resized_logo)
        self.logo_canvas.create_image(0, 0, image=self.final_logo, anchor='nw')

        # button territory
        self.play_online_btn = Button(self.root,
                                      bg='#008080',
                                      activebackground='#4ca6a6',
                                      width=self.widget_width // 17,
                                      text='Play Online',
                                      justify=CENTER,
                                      command=self.menu_controller.play_online)

        self.play_friend_btn = Button(self.root,
                                      bg='#008080',
                                      activebackground='#4ca6a6',
                                      width=self.widget_width // 17,
                                      text='Play with friend',
                                      justify=CENTER,
                                      command=self.menu_controller.play_friend)

        self.play_ai_btn = Button(self.root,
                                  bg='#008080',
                                  activebackground='#4ca6a6',
                                  width=self.widget_width // 17,
                                  text='Play with computer',
                                  justify=CENTER,
                                  command=self.menu_controller.play_computer)

        self.logo_canvas.place(x=self.view_width / 2 - (self.view_height // 3), y=0)
        self.play_online_btn.place(x=self.view_width / 2 - (self.view_height // 6), y=self.view_height * 14.5 / 20)
        self.play_friend_btn.place(x=self.view_width / 2 - (self.view_height // 6), y=self.view_height * 16 / 20)
        self.play_ai_btn.place(x=self.view_width / 2 - (self.view_height // 6), y=self.view_height * 17.5 / 20)


class BoardView:
    colors = ['#008200', '#00A000']

    def __init__(self, game_board_controller):
        self.game_board_controller = game_board_controller
        self.board = []
        self.root = self.game_board_controller.root

        self.view_height = self.root.winfo_screenheight() - 67
        self.view_width = self.view_height * 8 // 9
        self.square_size = self.view_width / 8

        self.lbl = Label(self.root,
                         text='Black',
                         font=('OpenSymbol', 15, 'bold'),
                         bg='#004c4c',
                         bd=0,
                         highlightthickness=0)

        self.my_disc = Canvas(self.root,
                              width=self.square_size,
                              height=self.square_size,
                              bg='#004c4c',
                              bd=0,
                              highlightthickness=0)

        self.rival_disc = Canvas(self.root,
                                 width=self.square_size,
                                 height=self.square_size,
                                 bg='#004c4c',
                                 bd=0,
                                 highlightthickness=0)
        for i in range(8):
            row = []
            for j in range(8):
                square = Canvas(self.root,
                                width=self.square_size,
                                height=self.square_size,
                                bg=self.colors[(i + j) % 2],
                                bd=0,
                                highlightthickness=0)
                square.bind('<Button-1>', self.game_board_controller.select)
                row.append(Square(square))
                square.place(x=j * self.square_size,
                             y=i * self.square_size + self.view_height / 9)
            self.board.append(row)

        self.lbl.pack(pady=self.square_size / 3)
        self.my_disc.place(x=0, y=0)
        self.rival_disc.place(x=self.square_size * 7, y=0)

    def set_disc(self, row, col, color):
        disc_padding = self.square_size / 20
        disc = self.board[row][col].square.create_oval(0 + disc_padding, 0 + disc_padding,
                                                       self.square_size - disc_padding, self.square_size - disc_padding,
                                                       fill=color, width=0)
        self.board[row][col].disc = disc

    def change_color(self, row, col, color):
        self.board[row][col].square.itemconfig(self.board[row][col].disc, fill=color)

    def init_header(self, color1, color2):
        disc_padding = self.square_size / 20
        self.my_disc.create_oval(0 + disc_padding, 0 + disc_padding,
                                 self.square_size - disc_padding, self.square_size - disc_padding,
                                 fill=color1, width=0)
        self.rival_disc.create_oval(0 + disc_padding, 0 + disc_padding,
                                    self.square_size - disc_padding, self.square_size - disc_padding,
                                    fill=color2, width=0)


class Square:
    def __init__(self, square):
        self.square = square
        self.disc = None
