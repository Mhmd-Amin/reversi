from res.views import *
import model
import helper
import tkinter.messagebox


class MenuController:
    def __init__(self):
        self.menu = Menu(self)
        self.root = self.menu.root
        self.logo_canvas = self.menu.logo_canvas
        self.play_online_btn = self.menu.play_online_btn
        self.play_friend_btn = self.menu.play_friend_btn
        self.play_computer_btn = self.menu.play_ai_btn

        self.root.mainloop()

    def play_online(self):
        helper.Helper.destroy(self.root.winfo_children())
        PlayOnlineController(self.root)

    def play_friend(self):
        helper.Helper.destroy(self.root.winfo_children())
        TwoPlayerController(self.root)

    def play_computer(self):
        pass


class MainController:
    def __init__(self, root):
        self.root = root

        self.game_board = BoardView(self)
        self.game_board.set_disc(3, 3, 'black')
        self.game_board.set_disc(4, 4, 'black')
        self.game_board.set_disc(3, 4, 'white')
        self.game_board.set_disc(4, 3, 'white')

    def set_lbl(self, color):
        self.game_board.lbl.configure(text=color.title(), foreground=color)

    def set_winner(self, winner):
        if winner == 'black':
            self.game_board.lbl.configure(text='Black win', foreground='black')
        elif winner == 'white':
            self.game_board.lbl.configure(text='White win', foreground='white')
        else:
            self.game_board.lbl.configure(text='Draw', foreground='#ffa500')

    @staticmethod
    def get_loc(widget):
        square_number = MainController.get_square_name(widget)
        square_number = int(square_number) - 4

        if square_number < 8:
            loc = (0, square_number % 8)
        elif square_number < 16:
            loc = (1, square_number % 8)
        elif square_number < 24:
            loc = (2, square_number % 8)
        elif square_number < 32:
            loc = (3, square_number % 8)
        elif square_number < 40:
            loc = (4, square_number % 8)
        elif square_number < 48:
            loc = (5, square_number % 8)
        elif square_number < 56:
            loc = (6, square_number % 8)
        else:
            loc = (7, square_number % 8)

        return loc

    @staticmethod
    def get_square_name(widget):
        widget_name = str(widget)
        return widget_name.split('s')[1]


class TwoPlayerController(MainController):
    def __init__(self, root):
        super().__init__(root)
        self.game = model.Game()

        self.game_board.init_header('black', 'white')

        self.root.mainloop()

    def select(self, event):
        square_loc = self.get_loc(event.widget)
        discs = self.game.put(square_loc)
        if discs is not None and len(discs) != 0 and self.game.winner is None:
            self.game_board.set_disc(discs[0][0], discs[0][1], self.game.turn)
            for conquered in discs[1]:
                self.game_board.change_color(conquered[0], conquered[1], self.game.turn)
            self.game.change_turn()
            self.set_lbl(self.game.turn)

        if self.game.winner is not None:
            lbl_text = self.game_board.lbl['text']
            if lbl_text == 'Black' or lbl_text == 'White':
                self.set_winner(self.game.winner)


class PlayOnlineController(MainController):
    def __init__(self, root):
        super().__init__(root)
        self.root.protocol("WM_DELETE_WINDOW", self.exit)

        self.connector = model.Connector(self)
        self.connector.connect()
        self.game = None

        while self.game is None:
            pass

        if self.game.my_disc == 'black':
            self.game_board.init_header('black', 'white')
        else:
            self.game_board.init_header('white', 'black')

        self.root.mainloop()

    def select(self, event):
        if self.game.turn == self.game.my_disc and self.game.winner is None:
            square_loc = self.get_loc(event.widget)
            self.connector.send({'put': square_loc})

    def check_data(self, data):
        if data is not None:
            keys = data.keys()
            if 'discs' in keys:
                self.game_board.set_disc(data['discs'][0][0], data['discs'][0][1], self.game.turn)
                for square in data['discs'][1]:
                    self.game_board.change_color(square[0], square[1], self.game.turn)
                self.game.turn = data['turn']
                self.set_lbl(data['turn'])
            elif 'result' in keys:
                self.game.winner = data['result']
                self.set_winner(data['result'])
                self.connector.close_connector()
            elif 'color' in keys:
                self.game = model.Game(data['color'])
                self.set_lbl(data['color'])

    def disconnect(self):
        self.connector.close()
        self.connector = None

    def exit(self):
        if self.game.winner is None:
            tkinter.messagebox.showwarning('warning', 'You most complete this game')
            return
        self.root.destroy()


MenuController()
