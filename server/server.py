import socket
import _pickle as c_pickle
import threading


class Server:
    def __init__(self):
        self._PORT = 9876
        self._ID = '127.0.0.1'

        self.server_socket = None

        self.want_to_play = []

    def start_server(self):
        self.server_socket = socket.socket()
        self.server_socket.bind((self._ID, self._PORT))
        self.server_socket.listen()
        threading.Thread(target=self.start_game).start()
        self.listen()

    def listen(self):
        print('listening...')
        while True:
            connection = self.server_socket.accept()
            print('connected')
            user = Client(connection)
            self.want_to_play.append(user)

    def start_game(self):
        while True:
            if len(self.want_to_play) >= 2:
                print('a game is started')
                player1 = self.want_to_play.pop(0)
                player1.disc_color = 'black'
                player2 = self.want_to_play.pop(0)
                player2.disc_color = 'white'
                game = Game(player1, player2)
                threading.Thread(target=game.start_game()).start()


class Client:
    def __init__(self, connection):
        self.connection = connection[0]
        self.disc_color = None

    def listen(self):
        data = c_pickle.loads(self.connection.recv(64))
        return data

    def send(self, data):
        self.connection.send(c_pickle.dumps(data, -1))


class Game:
    def __init__(self, player1, player2):
        self.player1 = player1
        self.player2 = player2

        self.board = None
        self.turn = None
        self.rival = None
        self.winner = None
        self.round = None

    def start_game(self):
        self.board = Board()
        self.turn = self.player1
        self.rival = self.player2

        self.round = 5
        self.turn.send({'color': 'black'})
        self.rival.send({'color': 'white'})

        while self.winner is None:
            request = self.turn.listen()
            discs = self.put_disc((request['put']))
            if discs is not None and len(discs) != 0:
                self.change_turn()
                response = {'discs': discs, 'turn': self.turn.disc_color}
                self.turn.send(response)
                self.rival.send(response)

        result = {'result': self.winner}
        self.turn.send(result)
        self.rival.send(result)
        self.turn.connection.close()
        self.rival.connection.close()

    def put_disc(self, square_loc):
        grabbed_line = []
        if self.board.is_none(square_loc):
            grabbed_line.extend(self.board.check_left(square_loc, self.turn.disc_color))
            grabbed_line.extend(self.board.check_right(square_loc, self.turn.disc_color))
            grabbed_line.extend(self.board.check_up(square_loc, self.turn.disc_color))
            grabbed_line.extend(self.board.check_down(square_loc, self.turn.disc_color))
            grabbed_line.extend(self.board.check_left_up(square_loc, self.turn.disc_color))
            grabbed_line.extend(self.board.check_left_down(square_loc, self.turn.disc_color))
            grabbed_line.extend(self.board.check_right_up(square_loc, self.turn.disc_color))
            grabbed_line.extend(self.board.check_right_down(square_loc, self.turn.disc_color))
            if len(grabbed_line) != 0:
                grabbed_discs = self.board.grab(grabbed_line, square_loc, self.turn.disc_color)
                return grabbed_discs

    def change_turn(self):
        if self.winner is None:
            self.turn, self.rival = self.rival, self.turn
            if not self.has_chance():
                self.turn, self.rival = self.rival, self.turn
            self.winner = self.is_end()
            self.round += 1

    def has_chance(self):
        for row in range(8):
            for col in range(8):
                if self.board.playground[row][col] is None:
                    if len(self.board.check_up((row, col), self.turn.disc_color)) != 0:
                        return True
                    elif len(self.board.check_left((row, col), self.turn.disc_color)) != 0:
                        return True
                    elif len(self.board.check_down((row, col), self.turn.disc_color)) != 0:
                        return True
                    elif len(self.board.check_right((row, col), self.turn.disc_color)) != 0:
                        return True
                    elif len(self.board.check_left_up((row, col), self.turn.disc_color)) != 0:
                        return True
                    elif len(self.board.check_right_up((row, col), self.turn.disc_color)) != 0:
                        return True
                    elif len(self.board.check_left_down((row, col), self.turn.disc_color)) != 0:
                        return True
                    elif len(self.board.check_right_down((row, col), self.turn.disc_color)) != 0:
                        return True
        return False

    def is_full(self):
        if self.round == 64:
            return True
        return False

    def is_end(self):
        if not self.board.has_disc(self.rival.disc_color):
            return self.turn.disc_color
        elif self.is_full():
            black_num, white_num = self.board.count_disc()
            winner = self.determine_winner(black_num, white_num)
            return winner

    @staticmethod
    def determine_winner(black_num, white_num):
        if black_num > white_num:
            return 'black'
        elif black_num < white_num:
            return 'white'
        else:
            return 'draw'


class Board:
    def __init__(self):
        self.playground = [[None for col in range(8)] for row in range(8)]
        self.playground[3][3] = 'black'
        self.playground[4][4] = 'black'
        self.playground[3][4] = 'white'
        self.playground[4][3] = 'white'

    def grab(self, grabbed_line, put_disc_loc, color):
        disc_loc = []
        for line_loc in grabbed_line:
            # horizontal
            if put_disc_loc[0] == line_loc[0]:
                if put_disc_loc[1] > line_loc[1]:
                    for disc_col in range(line_loc[1] + 1, put_disc_loc[1]):
                        disc_loc.append((put_disc_loc[0], disc_col))
                else:
                    for disc_col in range(put_disc_loc[1] + 1, line_loc[1]):
                        disc_loc.append((put_disc_loc[0], disc_col))
                continue
            elif put_disc_loc[1] == line_loc[1]:
                if put_disc_loc[0] > line_loc[0]:
                    for disc_row in range(line_loc[0] + 1, put_disc_loc[0]):
                        disc_loc.append((disc_row, put_disc_loc[1]))
                else:
                    for disc_row in range(put_disc_loc[0] + 1, line_loc[0]):
                        disc_loc.append((disc_row, put_disc_loc[1]))
                continue
            else:
                i = 1
                if put_disc_loc[0] > line_loc[0]:
                    if put_disc_loc[1] > line_loc[1]:
                        for disc_row in range(line_loc[0] + 1, put_disc_loc[0]):
                            disc_loc.append((disc_row, line_loc[1] + i))
                            i += 1
                    else:
                        for disc_row in range(line_loc[0] + 1, put_disc_loc[0]):
                            disc_loc.append((disc_row, line_loc[1] - i))
                            i += 1
                else:
                    if put_disc_loc[1] > line_loc[1]:
                        for disc_row in range(put_disc_loc[0] + 1, line_loc[0]):
                            disc_loc.append((disc_row, put_disc_loc[1] - i))
                            i += 1
                    else:
                        for disc_row in range(put_disc_loc[0] + 1, line_loc[0]):
                            disc_loc.append((disc_row, put_disc_loc[1] + i))
                            i += 1
        self.change_disc(put_disc_loc, color)
        for disc in disc_loc:
            self.change_disc(disc, color)

        return put_disc_loc, disc_loc

    def check_left(self, loc, color):
        grabbed_line = []
        if loc[1] > 1:
            for col in range(loc[1] - 1, -1, -1):
                if self.playground[loc[0]][col] == color:
                    if col != loc[1] - 1:
                        grabbed_line.append((loc[0], col))
                    break
                elif self.playground[loc[0]][col] is None:
                    break
        return grabbed_line

    def check_right(self, loc, color):
        grabbed_line = []
        if loc[1] < 6:
            for col in range(loc[1] + 1, 8):
                if self.playground[loc[0]][col] == color:
                    if col != loc[1] + 1:
                        grabbed_line.append((loc[0], col))
                    break
                elif self.playground[loc[0]][col] is None:
                    break
        return grabbed_line

    # vertical
    def check_up(self, loc, color):
        grabbed_line = []
        if loc[0] > 1:
            for row in range(loc[0] - 1, -1, -1):
                if self.playground[row][loc[1]] == color:
                    if row != loc[0] - 1:
                        grabbed_line.append((row, loc[1]))
                    break
                elif self.playground[row][loc[1]] is None:
                    break
        return grabbed_line

    def check_down(self, loc, color):
        grabbed_line = []
        if loc[0] < 6:
            for row in range(loc[0] + 1, 8):
                if self.playground[row][loc[1]] == color:
                    if row != loc[0] + 1:
                        grabbed_line.append((row, loc[1]))
                    break
                elif self.playground[row][loc[1]] is None:
                    break
        return grabbed_line

    def check_left_up(self, loc, color):
        grabbed_line = []
        if loc[0] != 0 and loc[1] != 0:
            row = loc[0] - 1
            col = loc[1] - 1
            while row >= 0 and col >= 0:
                if self.playground[row][col] == color:
                    if row != loc[0] - 1 and col != loc[1] - 1:
                        grabbed_line.append((row, col))
                    break
                elif self.playground[row][col] is None:
                    break
                row -= 1
                col -= 1
        return grabbed_line

    def check_left_down(self, loc, color):
        grabbed_line = []
        if loc[0] != 7 and loc[1] != 0:
            row = loc[0] + 1
            col = loc[1] - 1
            while row <= 7 and col >= 0:
                if self.playground[row][col] == color:
                    if row != loc[0] + 1 and col != loc[1] - 1:
                        grabbed_line.append((row, col))
                    break
                elif self.playground[row][col] is None:
                    break
                row += 1
                col -= 1
        return grabbed_line

    def check_right_up(self, loc, color):
        grabbed_line = []
        if loc[0] != 0 and loc[1] != 7:
            row = loc[0] - 1
            col = loc[1] + 1
            while row >= 0 and col <= 7:
                if self.playground[row][col] == color:
                    if row != loc[0] - 1 and col != loc[1] + 1:
                        grabbed_line.append((row, col))
                    break
                elif self.playground[row][col] is None:
                    break
                row -= 1
                col += 1
        return grabbed_line

    def check_right_down(self, loc, color):
        grabbed_line = []
        if loc[0] != 7 and loc[1] != 7:
            row = loc[0] + 1
            col = loc[1] + 1
            while row <= 7 and col <= 7:
                if self.playground[row][col] == color:
                    if row != loc[0] + 1 and col != loc[1] + 1:
                        grabbed_line.append((row, col))
                    break
                elif self.playground[row][col] is None:
                    break
                row += 1
                col += 1
        return grabbed_line

    def has_disc(self, disc_color):
        for row in range(7):
            for col in range(7):
                if self.playground[row][col] == disc_color:
                    return True
        return False

    def count_disc(self):
        black_num, white_num = 0, 0

        for row in range(8):
            for col in range(8):
                if self.playground[row][col] == 'black':
                    black_num += 1
                elif self.playground[row][col] == 'white':
                    white_num += 1
        return black_num, white_num

    def is_none(self, square_loc):
        if self.playground[square_loc[0]][square_loc[1]] is None:
            return True
        return False

    def change_disc(self, disc_loc, color):
        self.playground[disc_loc[0]][disc_loc[1]] = color


server = Server()
server.start_server()
