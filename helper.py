class Helper:
    @staticmethod
    def destroy(widgets: list) -> None:
        """ destroy widget """
        for widget in widgets:
            widget.destroy()
